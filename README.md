
[![pipeline status](https://gitlab.com/adscape-dev/version-template/badges/master/pipeline.svg)](https://gitlab.com/adscape-dev/version-template/commits/master)

# Contents
- [Contents](#contents)
- [How to use](#how-to-use)
- [Credits](#credits)
- [License](#license)

# How to use

1. Add the dependency to the `pom.xml`
```
<dependency>
  <groupId>de.adscape.core</groupId>
  <artifactId>version-template</artifactId>
  <version>1.0.0</version>
</dependency>
```

2. Add the `build-info` goal to the `build` section of the `pom.xml` 

```
<build>
  <plugins>
    <plugin>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-maven-plugin</artifactId>

      [...]

      <executions>
        <execution>
          <id>build-info</id>
          <goals>
            <goal>build-info</goal>
          </goals>
        </execution>
      </executions>

      [...]

    </plugin>
  </plugins>
</build>
```

3. You're done! Try to get the version from `localhost:8080/version`

```
<Version>
  <name>expert-partner-portal-backend</name>
  <version>1.5-SNAPSHOT</version>
  <environment>local</environment>
  <buildTime>2019-09-26T07:59:58.357Z</buildTime>
</Version>
```

# Credits
The idea of accessing the Spring Boot Build Properties comes from [Vojtech Ruzicka](https://www.vojtechruzicka.com/spring-boot-version/)

# License

```
MIT License

Copyright (c) 2019-present, ADSCAPE GmbH

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```