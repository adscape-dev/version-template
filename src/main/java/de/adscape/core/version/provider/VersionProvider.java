package de.adscape.core.version.provider;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.info.BuildProperties;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import de.adscape.core.version.dto.Version;


/**
 * @author jdickel
 * @since 25.09.2019 21:43:26
 *
 * ADSCAPE GmbH Siegen
 */

@Service
public class VersionProvider {
	
	private static final String DEFAULT_PROFILE = "default";

	@Autowired
	BuildProperties buildProperties;
	
	@Autowired
	Environment environment;

	public Version getCurrentVersion() {
		return Version.builder()
			.buildTime(buildProperties.getTime().toString())
			.environment(getActiveProfile())
			.name(buildProperties.getName())
			.version(buildProperties.getVersion())
			.build();
	}
	
	private String getActiveProfile() {
		return environment.getActiveProfiles().length > 0 ? environment.getActiveProfiles()[0] : DEFAULT_PROFILE;
	}
	
}
