package de.adscape.core.version.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import de.adscape.core.version.dto.Version;
import de.adscape.core.version.provider.VersionProvider;

/**
 * @author jdickel
 * @since 25.09.2019 21:53:24
 *
 * ADSCAPE GmbH Siegen
 */

@RestController
@RequestMapping("version")
public class RestVersionController {

	@Autowired
	VersionProvider versionProvider;

	@GetMapping("")
	public Version getVersion() {
		return versionProvider.getCurrentVersion();
	}
}
