package de.adscape.core.version.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author jdickel
 * @since 25.09.2019 21:50:07
 *
 * ADSCAPE GmbH Siegen
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Version {
	
	private String name;
	private String version;
	private String environment;
	private String buildTime;
	
}
